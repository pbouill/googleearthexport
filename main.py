from openpyxl import load_workbook
import pickle
import hashlib
from datetime import datetime
import tempfile

from data.instrument import Instrument
from data.station import Station
from data.site import Site

from simplekml import kml

from util.logging import getLogger
logger = getLogger()


FILE_DIR = tempfile.gettempdir() + '/googleearthexport/'
FILE_NAME = "H360332-00000-270-216-0001"
CACHE_EXT = ".cache"
FILE_EXT = ".xlsx"
INST_LIST = "InstList"
STN_LIST = "StnList"

# create the list of headers for the instrument list, column index values set to "None" here, but are populated once
# the raw file is read
H_INST_LIST = {}
H_STN_LIST = {}


def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def read(filename=FILE_NAME):
    logger.info("Calculating raw file checksum (MD5) for file '%s'..." %(filename))
    checksum = md5(filename + FILE_EXT)  # calculate the checksum of the source file
    logger.info('... %s' % checksum)
    try:
        raw_data = pickle.load(open(filename + CACHE_EXT, 'rb'))  # read the python object pickle, if it exists...
        logger.info("Reading stored/pickled objects from cache file '%s', inspecting checksum..." % (filename))
        p_checksum = raw_data['checksum']  # get the checksum stored previously in the cache/pickle
        logger.info('...%s' %(p_checksum))
        if not checksum == p_checksum:
            logger.warning('Checksums do not match.')
            raise ValueError('File and Cached checksums do not match.')
        logger.info('Cache file read successfully.')
        update_col_idx(raw_data) # update the column index maps
    except (FileNotFoundError, KeyError, ValueError) as e:
        raw_data = read_sourcefile(filename, checksum)
    return raw_data


def read_sourcefile(filename=FILE_NAME, checksum=None):
    logger.info('Creating new cache object, reading the raw file...')
    if checksum is None:  # get the raw file checksum if not already provided to us
        checksum = md5(filename + FILE_EXT)

    raw_data = {'checksum': checksum, 'instrument list': None, 'station list': None}  # instantiate our pickle structure
    ts = datetime.now()
    wb = load_workbook(filename=filename + FILE_EXT, keep_links=False, data_only=True) #, read_only=True)
    logger.info('%s workbook loaded in... %s' %(filename+FILE_EXT, (datetime.now() - ts)))
    for ws in wb.worksheets:
        for tbl_nm, ref in ws.tables.items():
            if tbl_nm == INST_LIST:
                raw_data['instrument list'] = ws[ref]
            elif tbl_nm == STN_LIST:
                raw_data['station list'] = ws[ref]
            if not ((raw_data['instrument list'] is None) or (raw_data['station list'] is None)):
                break
    wb.close()

    update_col_idx(raw_data) # update the column index maps
    with open(filename + CACHE_EXT, 'wb') as f:
        pickle.dump(raw_data, f)  # pickle all of our hard work for easy access
    return raw_data


def update_col_idx(raw_data):
    logger.info('Updating the column header index maps...')
    il = raw_data['instrument list']
    il_headers = [cell.value for cell in il[0]]
    for k, v in Instrument.column_map.items():
        H_INST_LIST[k] = il_headers.index(v)

    logger.debug('...instrument list: %s' %(H_INST_LIST))

    sl = raw_data['station list']
    sl_headers = [cell.value for cell in sl[0]]
    for k, v in Station.column_map.items():
        H_STN_LIST[k] = sl_headers.index(v)

    logger.debug('...station list: %s' % (H_STN_LIST))


def getRowValue(row, header, col_dict):
    return row[col_dict[header]].value


def parseRowDict(row, coldict):
    rowdict = {}  # instantiate our dictionary to create our instrument
    for header in coldict:
        rv = getRowValue(row, header, coldict)
        if rv:
            rowdict[header] = rv
    return rowdict


def addStations(station_list, sites={}, site_sel=None):
    logger.info('Adding stations from the raw data list.')

    for r in station_list[1:]: # iterate over the station list, skip the header row though
        kwargs = parseRowDict(r, H_STN_LIST)
        site_name = kwargs.pop('site_name', 'Unallocated')

        if site_sel is not None:
            if site_name not in site_sel:
                continue  # if we have a list to filter against, and we don't match, move on to the next row

        if not site_name in sites:
            sites[site_name] = Site(name=site_name)

        site = sites[site_name]
        kwargs['site'] = site  # add the site object as a keyword argument
        if not 'name' in kwargs:
            kwargs['name'] = 'Unallocated'
        if not kwargs['name'] == 'Total':
            logger.debug('Creating new station for site [%s] with keyword arguments: %s' %(site_name, kwargs))
            Station(**kwargs)  # automatically adds the station to the site with 'site' keyword argument
    return sites


def addInstruments(instrument_list, sites={}, site_sel=None):
    logger.info('Adding instruments from the raw data list.')
    for r in instrument_list[1:]: # skip the first (header row)
        kwargs = parseRowDict(r, H_INST_LIST)

        site_name = kwargs.pop('site_name', 'Unallocated')
        station_name = kwargs.pop('station_name', 'Unallocated')

        if site_sel is not None:
            if site_name not in site_sel:
                continue  # if we have a list to filter against, and we don't match, move on to the next row

        if not 'name' in kwargs:
            logger.info('No tag specified for %s' %kwargs)
            if 'old_tag' in kwargs:
                logger.info('...using "old_tag" as instrument tag: %s' %kwargs['old_tag'])
                kwargs['name'] = kwargs['old_tag']
            else:
                kwargs['name'] = 'Instrument (Unlabelled)'

        if not kwargs['name'] == 'Total': # ignore the totals row!
            if not site_name in sites:
                logger.warning('"%s" not found in site list (%s). Creating new site now...' %(site_name, sites.keys()))
                sites[site_name] = Site(name=site_name)
            else:
                logger.info('Will attach instrument "%s" to existing site in site list: %s' %(kwargs, site_name))
            site = sites[site_name]

            if not station_name in site.stations:
                logger.warning('"%s" not found in site station list (%s). Creating new station now...' % (station_name, site.stations.keys()))
                station = Station(name=station_name, site=site)

            else:
                station = site.stations[station_name]
            logger.debug('adding new instrument with keyword arguments: %s' % kwargs)
            Instrument(**kwargs, station=station)  # automatically adds to the station with the 'station' keyword arg

    return sites


def addSites(sites, kml_obj=None):
    if kml_obj is None:
        kml_obj = kml.Kml()
    for s in sites.values():
        if s not in kml_obj.document._features:
            kml_obj.document._features.append(s)
    return kml_obj


if __name__ == '__main__':

    sel = None
    # sel = ['Coleman (Levack)']

    raw_data = read(filename=FILE_DIR + FILE_NAME)

    sites = addStations(raw_data['station list'], site_sel=sel)
    sites = addInstruments(raw_data['instrument list'], sites=sites, site_sel=sel)

    kml_file = addSites(sites)

    kml_file.document.name = FILE_NAME
    kml_file.save(FILE_DIR + FILE_NAME+'.kml')
    kml_file.savekmz(FILE_DIR + FILE_NAME+".kmz")