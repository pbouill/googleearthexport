import coloredlogs
import logging
import inspect
from pathlib import Path

PKG_BASE_PATH = Path(__file__).parent.parent.parent
DEFAULT_FORMAT = '%(asctime)-20s %(levelname)-8s %(name)s:%(lineno)d %(funcName)s() ::  %(message)s'

DEFAULT_FIELD_STYLES = coloredlogs.DEFAULT_FIELD_STYLES.copy()
DEFAULT_FIELD_STYLES.update(coloredlogs.parse_encoded_styles('funcName=cyan'))

coloredlogs.install(level=logging.DEBUG, fmt=DEFAULT_FORMAT, field_styles=DEFAULT_FIELD_STYLES)


def getLogger():
    stack = inspect.stack()
    module_path = Path(stack[1][1]).relative_to(PKG_BASE_PATH).with_suffix('')
    l = logging.getLogger('.'.join(module_path.parts))
    return l