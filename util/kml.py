from xml.etree import ElementTree as ET
import types

from util.logging import getLogger
logger = getLogger()


def xml_base_cls_str(self):  # need to override to provide the proper xml tags...
        base_cls = self.__class__.__base__.__name__  # get the base class name (consistent with kml node tags)
        node = ET.fromstring(self.__class__.__base__.__str__(self))  # get the raw xml returned from the base class
        node.tag = base_cls  # set the node tag the base class name
        s = ET.tostring(node).decode()
        return s


def get__str__func(obj):
    return types.MethodType(xml_base_cls_str, obj)