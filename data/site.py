from simplekml.featgeom import Folder as Folder
from data.station import Station

import util.kml

from util.logging import getLogger
logger = getLogger()


class Site(Folder):
    # __name__ = Folder.__name__
    def __init__(self, **kwargs):
        self.__children = None
        self.__str__ = util.kml.get__str__func(self)

        self.stations = kwargs.pop('stations', {})

        super().__init__(**kwargs)
        # self.stations_folder = self.newfolder(name='Stations')
        self.__station_instruments = self.newfolder(name='Instruments')
        self.group_folder = self.newfolder(name='Station Groups')
        self.groups = {}

    def add_station(self, station):
        if station not in self.stations.values():
            self.stations[station.name] = station
        if station.site is not self:
            station.site = self
        if station not in self.__station_instruments._features:
            self.__station_instruments._features.append(station)
            logger.info('%s: adding station instrument folder: %s' %(self.name, station))
        if station.group:
            # fldr = self.group_folder
            if not station.group in self.groups:
                self.groups[station.group] = self.group_folder.newfolder(name=station.group)
            fldr = self.groups[station.group]
        else:
            fldr = self
        if (station.point.placemark not in fldr._features) and station.has_location:
            fldr._features.append(station.point.placemark)
            logger.info('%s: adding station placemark: %s' % (self.name, station.point.placemark))
        else:
            logger.warning('%s: placemark for %s not set (%s, %s)' %(self.name, station.name, station.has_location, station.point.latlon))

    def remove_station(self, station):
        for k, v in self.stations:
            if v == station:
                self.stations.pop(k)
                break
        if station.site is self:
            station.site = None
        if station in self.__station_instruments._features:
            self.__station_instruments._features.remove(station)
        if station.point.placemark in self._features:
            self._features.remove(station.point.placemark)

        # if isinstance(station, Station):
        #     station._parent = self
        #     self.stations[station.name] = station
        #     self._features.append(station)
        #     # print(station.point.placemark)
        #     # self._features.append(station.point)
        #     # print(station)
        #     if len(station.point.coords._coords) == 2:
        #         self._features.append(station.point.placemark)

    @property
    def stations(self):
        return self.__children

    @stations.setter
    def stations(self, stations):
        self.__children = stations
        for k, v in self.stations:
            if v.site is not self:
                v.site = self
