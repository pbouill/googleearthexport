from data.point import UTMPoint
from simplekml import Icon #, LabelStyle

# from xml.etree import ElementTree as ET

# from util.style import get_style
from util.logging import getLogger
logger = getLogger()
logger.setLevel('INFO')

# TODO: consider implementing simplekml.MultiGeometry(geometries=(), **kwargs) for connecting line to station
class Instrument(UTMPoint):
    column_map = {
        'name': 'Instrument Tag',
        'site_name': 'Site',
        'station_name': 'Station Reference',
        'utmZ': 'UTM NAD83 Zone',
        'utmE': 'UTM NAD83 Easting',
        'utmN': 'UTM NAD83 Northing',
        'inst_type': 'Instrument Type',
        'ifce_type': 'Interface Type',
        'is_new': 'Project to Procure/Install',
        'is_damaged': 'Damaged',
        'is_new_from_field_visit': 'New from Field Visit',
        'old_tag': 'Old Instrument Tag',
        'desc': 'Description',
        'guid': 'Unique ID Tag',
        'remarks': 'Remarks'
    }

    VWP_ICON = Icon(href='icons/instruments/vwp.png')
    DIGITAL_ICON = Icon(href='icons/instruments/digital.png')
    MANUAL_ICON = Icon(href='icons/instruments/manual.png')

    def __init__(self, **kwargs):
        self.__initializing = True
        self.__parent = None
        self.__inst_type = None
        self.__old_tag = None
        self.__desc = None
        self.__guid = None
        self.__interface_type = None
        # self.__is_new = None
        # self.__is_damaged = None
        # self.__is_new_from_field_visit = None
        self.__remarks = None
        self.connection_line = None

        inst_type = kwargs.pop('inst_type', None)
        old_tag = kwargs.pop('old_tag', None)
        desc = kwargs.pop('desc', None)
        guid = kwargs.pop('guid', None)
        interface_type = kwargs.pop('ifce_type', None)
        is_new = kwargs.pop('is_new', False)
        is_damaged = kwargs.pop('is_damaged', False)
        is_new_from_field_visit = kwargs.pop('is_new_from_field_visit', False)
        remarks = kwargs.pop('remarks', None)
        station = kwargs.pop('station', None)

        super().__init__(**kwargs)

        self.station = station
        self.interface_type = interface_type
        self.inst_type = inst_type
        self.old_tag = old_tag
        self.desc = desc
        self.guid = guid
        self.is_new = is_new
        self.is_damaged = is_damaged
        self.is_new_from_field_visit = is_new_from_field_visit
        self.remarks = remarks

        # self.__initializing = False
        self.update_style()

    @property
    def interface_type(self):
        return self.__interface_type

    @interface_type.setter
    def interface_type(self,ifce_type):
        self.__interface_type = ifce_type
        self.update_style()

    @property
    def is_new(self):
        return self.__is_new

    @is_new.setter
    def is_new(self,is_new):
        self.__is_new = is_new
        # self.update_style()

    @property
    def is_damaged(self):
        return self.__is_damaged

    @is_damaged.setter
    def is_damaged(self, is_damaged):
        self.__is_damaged = is_damaged
        # self.update_style()

    @property
    def is_new_from_field_visit(self):
        return self.__is_new_from_field_visit

    @is_new_from_field_visit.setter
    def is_new_from_field_visit(self, is_new_from_field_visit):
        self.__is_new_from_field_visit = is_new_from_field_visit
        # self.update_style()

    def _getdescription(self):
        descL = []
        if self.inst_type:
            descL.append('<b>Type:</b> %s' % (self.inst_type))
        if self.old_tag:
            descL.append('<b>Old Tag:</b> %s' %(self.old_tag))
        if self.desc:
            descL.append('<b>Description:</b> %s' %(self.desc))
        if self.remarks:
            descL.append('<b>Remarks:</b> %s' % (self.remarks))
        if self.guid:
            descL.append('<b>GUID:</b> %s' % (self.guid))
        return '<br>'.join(descL)

    @property
    def guid(self):
        return self.__guid

    @guid.setter
    def guid(self, guid):
        self.__guid = guid
        self.description = self._getdescription()

    @property
    def old_tag(self):
        return self.__old_tag

    @old_tag.setter
    def old_tag(self, old_tag):
        self.__old_tag = old_tag
        self.description = self._getdescription()

    @property
    def desc(self):
        return self.__desc

    @desc.setter
    def desc(self, desc):
        self.__desc = desc
        self.description = self._getdescription()

    @property
    def inst_type(self):
        return self.__inst_type

    @inst_type.setter
    def inst_type(self, inst_type):
        self.__inst_type = inst_type
        self.description = self._getdescription()

    @property
    def remarks(self):
        return self.__remarks

    @remarks.setter
    def remarks(self, remarks):
        self.__remarks = remarks
        self.description = self._getdescription()

    # TODO: Need to update this...
    @property
    def parent(self):
        return self.__parent

    @parent.setter
    def parent(self, parent):
        if (self.station is not parent) and self.station is not None:
            self.station.remove_instrument(self)
        self.__parent = parent
        if self.parent is not None:
            self.station.add_instrument(self)

    @property
    def _parent(self):
        return self.parent

    @_parent.setter
    def _parent(self, parent):
        self.parent = parent

    @property
    def station(self):
        return self.parent

    @station.setter
    def station(self, station):
        self.parent = station

    def update_style(self):
        if self.inst_type is not None:
            if self.inst_type.upper().find('VWP') >= 0:
                self.style.iconstyle.icon = self.__class__.VWP_ICON
            elif self.inst_type.upper().find('MANUAL') >= 0:
                self.style.iconstyle.icon = self.__class__.MANUAL_ICON
            else:
                self.style.iconstyle.icon = self.__class__.DIGITAL_ICON
