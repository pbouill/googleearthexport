from simplekml.featgeom import Folder, Icon
from data.point import UTMPoint

# from util.style import get_style
import util.kml


from util.logging import getLogger
logger = getLogger()


class Station(Folder):
    column_map = {
        'name': 'Station Tag',
        'site_name': 'Site',
        'utmZ': 'UTM NAD83 Zone',
        'utmE': 'UTM NAD83 Easting',
        'utmN': 'UTM NAD83 Northing',
        'summary': 'Summary',
        'guid': 'Unique ID Tag',
        'uplink': 'Uplink',
        'dl': 'Datalogger',
        'comments': 'Comments',
        'group': 'Group'
    }

    DLG01_Icon = Icon(href='icons/stations/DLG01.png')
    DLG01_copper_Icon = Icon(href='icons/stations/DLG01_copper.png')
    DLG01_cellular_Icon = Icon(href='icons/stations/DLG01_copper_cellular.png')
    DLG01_copper_cellular_Icon = Icon(href='icons/stations/DLG01_copper_cellular.png')

    DLG02_Icon = Icon(href='icons/stations/DLG02.png')
    DLG02_copper_Icon = Icon(href='icons/stations/DLG02_copper.png')
    DLG02_cellular_Icon = Icon(href='icons/stations/DLG02_copper_cellular.png')
    DLG02_copper_cellular_Icon = Icon(href='icons/stations/DLG02_copper_cellular.png')

    AVW200_Icon = Icon(href='icons/stations/DLG02_copper_cellular.png')

    def __init__(self, **kwargs):
        self.__initializing = True
        self.__parent = None
        self.__children = None
        self.__guid = None
        self.__summary = None
        self.__uplink = None
        self.__dl = None
        self.__comments = None
        self.point = None
        self.__str__ = util.kml.get__str__func(self)

        summary = kwargs.pop('summary', None)
        guid = kwargs.pop('guid', None)
        uplink = kwargs.pop('uplink', None)
        dl = kwargs.pop('dl', None)
        comments = kwargs.pop('comments', None)
        site = kwargs.pop('site', None)

        self.group = kwargs.pop('group', None)
        self.instruments = kwargs.pop('instruments', [])


        self.point = UTMPoint(**kwargs)

        kwargs.pop('utmE', None)
        kwargs.pop('utmN', None)
        kwargs.pop('utmZ', None)


        super().__init__(**kwargs)
        self.visibility = not self.is_future

        self.site = site
        self.summary = summary
        self.guid = guid
        self.uplink = uplink
        self.datalogger = dl
        self.comments = comments

        self.visibility = not self.is_future

        self.__initializing = False
        self.update_style()

    @property
    def utmE(self):
        return self.point.utmE
    @utmE.setter
    def utmE(self, utmE):
        self.point.utmE = utmE

    @property
    def utmN(self):
        return self.point.utmN
    @utmN.setter
    def utmN(self, utmN):
        self.point.utmN = utmN

    @property
    def utmZ(self):
        return self.point.utmZ
    @utmZ.setter
    def utmZ(self, utmZ):
        self.point.utmZ = utmZ

    @property
    def utmZN(self):
        return self.point.utmZN
    @utmZN.setter
    def utmZN(self, utmZN):
        self.point.utmZN = utmZN

    @property
    def utm(self):
        return self.point.utm
    @utm.setter
    def utm(self, utmL):
        addPnt = not self.point.placemark in self._features  # flag if location not currently set
        self.point.utm = utmL
        if (self._parent is not None) and addPnt:
            self._parent._features.append(self.point.placemark)

    @property
    def latlon(self):
        return self.point.latlon

    @property
    def lat(self):
        return self.point.lat

    @property
    def lon(self):
        return self.point.lon


    def _getdescription(self):
        descL = []
        if self.summary:
            descL.append('<b>Summary:</b> %s' %(self.summary))
        if self.comments:
            descL.append('<b>Comments:</b> %s' % (self.comments))
        if self.guid:
            descL.append('<b>GUID:</b> %s' %(self.guid))
        return '<br>'.join(descL)

    @property
    def guid(self):
        return self.__guid

    @guid.setter
    def guid(self, guid):
        self.__guid = guid
        desc = self._getdescription()
        self.description = desc
        self.point.description = desc

    @property
    def summary(self):
        return self.__summary

    @summary.setter
    def summary(self, summary):
        self.__summary = summary
        desc = self._getdescription()
        self.description = desc
        self.point.description = desc

    @property
    def uplink(self):
        return self.__uplink
    @uplink.setter
    def uplink(self,uplink):
        self.__uplink = uplink
        self.update_style()

    @property
    def datalogger(self):
        return self.__dl
    @datalogger.setter
    def datalogger(self,dl):
        self.__dl = dl
        self.update_style()

    @property
    def comments(self):
        return self.__comments

    @comments.setter
    def comments(self, comments):
        self.__comments = comments
        self.description = self._getdescription()

    @property
    def has_location(self):
        return self.point.has_location

    @property
    def instruments(self):
        return self.__children

    @instruments.setter
    def instruments(self, instruments):
        self.__children = instruments
        for i in instruments:
            i.parent = self

    @property
    def parent(self):
        return self.__parent

    @parent.setter
    def parent(self, parent):
        """
        Attaches the station to the parent (site)
        - First remove from previous parent
        - Add to new parent
        """
        if parent is None:
            logger.info('%s: setting site to nothing...' %self.name)
        else:
            logger.info('%s: setting parent to "%s"' %(self.name, parent.name))
        if (self.site is not parent) and (self.site is not None):  # don't do anything if not required...
            self.site.remove_station(self)
        self.__parent = parent  # set our new parent (site)
        if self.site is not None:
            self.site.add_station(self)

    @property
    def _parent(self):
        return self.parent

    @_parent.setter
    def _parent(self, parent):
        self.parent = parent

    @property
    def site(self):
        return self.__parent

    @site.setter
    def site(self, site):
        self.parent = site
        # if self.site is not None:
        #     self.site.addStation(self)

    def add_instrument(self, instrument):
        if instrument not in self.instruments:
            self.instruments.append(instrument)
        if instrument.station is not self:
            instrument.station = self
        if instrument.placemark not in self._features:
            logger.info('%s: adding instrument "%s" placemark to folder' %(self.name, instrument.name))
            self._features.append(instrument.placemark)
            if self.has_location and instrument.has_location:
                instrument.connection_line = self.newlinestring(name=instrument.name)
                logger.info('%s %s' %(instrument.coords._coords, self.point.coords._coords))
                instrument.connection_line.coords = [instrument.coords._coords[0], self.point.coords._coords[0]]
                instrument.connection_line.visibility = not instrument.is_future

    def remove_instrument(self, instrument):
        if instrument in self.instruments:
            self.instruments.remove(instrument)
        if instrument.station is self:
            instrument.station = None
        if instrument.placemark in self._features:
            self._features.remove(instrument.placemark)
        if instrument.connection_line in self._features:
            self._features.remove(instrument.connection_line)

    @property
    def is_future(self):
        return self.point.is_future

    def update_style(self):
        if self.datalogger is not None:
            if self.datalogger.upper().find('DLG01') >= 0:
                if self.uplink is not None:
                    if self.uplink.upper().find('COPPER+CELLULAR') >= 0:
                        self.point.placemark.iconstyle.icon = self.__class__.DLG01_copper_cellular_Icon
                    elif self.uplink.upper().find('COPPER') >= 0:
                        self.point.placemark.iconstyle.icon = self.__class__.DLG01_copper_Icon
                    elif self.uplink.upper().find('CELLULAR') >= 0:
                        self.point.placemark.iconstyle.icon = self.__class__.DLG01_cellular_Icon
                else:
                    self.point.iconstyle.icon = self.__class__.DLG01_Icon
            elif self.datalogger.upper().find('DLG02') >= 0:
                if self.uplink is not None:
                    if self.uplink.upper().find('COPPER+CELLULAR') >= 0:
                        self.point.placemark.iconstyle.icon = self.__class__.DLG02_copper_cellular_Icon
                    elif self.uplink.upper().find('COPPER') >= 0:
                        self.point.placemark.iconstyle.icon = self.__class__.DLG02_copper_Icon
                    elif self.uplink.upper().find('CELLULAR') >= 0:
                        self.point.placemark.iconstyle.icon = self.__class__.DLG02_cellular_Icon
                    else:
                        self.point.placemark.iconstyle.icon = self.__class__.DLG02_Icon
                else:
                    self.point.placemark.iconstyle.icon = self.__class__.DLG02_Icon
        else:
            self.point.placemark.style.iconstyle.icon = self.__class__.AVW200_Icon
