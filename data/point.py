from simplekml import Point, Color
import utm

# from util.style import STYLES
from util.logging import getLogger
logger = getLogger()

class UTMPoint(Point):
    def __init__(self, **kwargs):
        self.__parent = None
        if 'utmE' in kwargs:
            utmE = kwargs.pop('utmE', None)
        if 'utmN' in kwargs:
            utmN = kwargs.pop('utmN', None)
        if 'utmZ' in kwargs:
            utmZ = kwargs.pop('utmZ', None)
        elif ('utmZN', 'utmZL') in kwargs:
            utmZN = kwargs.pop('utmZN', None)
            utmZL = kwargs.pop('utmZL', None)
            utmZ = utmZN + utmZL

        if 'parent' in kwargs:
            self.parent = kwargs.pop('parent')

        super().__init__(**kwargs)
        self.visibility = not self.is_future


        try:
            utmL = [utmE, utmN, utmZ]
            self.utm = utmL
        except UnboundLocalError:
            logger.warning('Point (%s) initialized without UTM coordinates' %(self.name))

        # self.utm = [utmE, utmN, utmZ]

    @property
    def utmE(self):
        return utm.from_latlon(*self.latlon)[0]

    @property
    def utmN(self):
        return utm.from_latlon(*self.latlon)[1]

    @property
    def utmZ(self):
        return ''.join(map(str,utm.from_latlon(*self.latlon)[2:]))

    @property
    def utmZN(self):
        return utm.from_latlon(*self.latlon)[2]

    @property
    def utmZL(self):
        return utm.from_latlon(*self.latlon)[3]

    @property
    def latlon(self):
        if self.has_location:
            return self.coords._coords[0][:-1]
        return None

    @property
    def lat(self):
        return self.latlon[0]

    @property
    def lon(self):
        return self.latlon[1]

    @property
    def utm(self):
        return utm.from_latlon(*self.latlon)

    @utm.setter
    def utm(self, utmL):
        if len(utmL) == 3:
            utmZ = self.parseUTMZ(utmL[2])
            utmL = utmL[:2]
            utmL = utmL+list(utmZ)
        elif not len(utmL) == 4:
            raise ValueError('UTM coordinates must be supplied as: Easing, Northing, Zone Number, Zone Letter')
        self.coords = [tuple(reversed(utm.to_latlon(*utmL)))]

    @utmE.setter
    def utmE(self, utmE):
        if not isinstance(utmE,(int, float)):
            raise ValueError('Easting coordinate must be a number')
        utmL = list(self.utm)
        utmL[0] = utmE
        self.utm = utmL

    @utmN.setter
    def utmN(self, utmN):
        if not isinstance(utmN,(int, float)):
            raise ValueError('Northing coordinate must be a number')
        utmL = list(self.utm)
        utmL[1] = utmN
        self.utm = utmL

    @utmZN.setter
    def utmZN(self, utmZN):
        if not isinstance(utmZN, int):
            raise ValueError('Zone number must be an integer')
        utmL = list(self.utm)
        utmL[2] = utmZN
        self.utm = utmL

    @utmZ.setter
    def utmZ(self, utmZ):
        utmZ = self.parseUTMZ(utmZ)
        utmL = list(self.utm[:2]+utmZ)
        self.utm = utmL

    def parseUTMZ(self,utmZ):
        if not isinstance(utmZ, str):
            raise ValueError('UTM Zone must be supplied as a string "{Zone Number}{Zone Letter}"')
        utmZL = utmZ[-1]
        if not (isinstance(utmZL, str) and (len(utmZL) == 1) and utmZL.isalpha()):
            raise ValueError('Zone letter must be A-Z')
        utmZN = utmZ[:-1]
        try:
            utmZN = int(utmZN)
        except:
            raise ValueError('Zone number must be an integer')

        return utmZN, utmZL

    @property
    def parent(self):
        return self.__parent

    @parent.setter
    def parent(self, parent):
        if self.parent is not None:
            if self.parent is not parent:
                self.parent.children.remove(self)
        self.__parent = parent
        if self not in self.parent.children:
            self.parent.append(self)

    @property
    def _parent(self):
        return self.__parent

    @_parent.setter
    def _parent(self, parent):
        self.__parent = parent

    @property
    def has_location(self):
        r = len(self.coords._coords) >= 1
        logger.debug('%s: checking if we have coordinates... %s (%s)' % (self.name, r, self.coords._coords))
        return r

    @property
    def is_future(self):
        if isinstance(self.name, str):
            return self.name[-1] == "*"

    # def update_style(self, item_type=None, future=None, new=None, ):
    #     logger.info('updating placemark style for object: %s' %self)
