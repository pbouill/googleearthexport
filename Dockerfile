FROM python:latest

RUN git clone https://bitbucket.org/pbouill/googleearthexport.git
WORKDIR googleearthexport

RUN pip install --no-cache-dir -r requirements.txt

CMD [ "python", "./main.py" ]